FROM golang:1.22.1-bullseye AS base

WORKDIR /src
COPY ../../common /usr/local/go/src/common
COPY ../../warehouse-executor .
RUN --mount=type=cache,target=/go/pkg/mod go mod download

FROM base AS build
ENV CGO_ENABLED=0
RUN go mod tidy
RUN --mount=type=cache,target=/go/pkg/mod --mount=type=cache,target=/root/.cache/go-build \
    go build \
        -o /go/bin/ \
        .

FROM gcr.io/distroless/static
COPY --from=build /go/bin/warehouse-executor /bin/warehouse-executor
ENTRYPOINT [ "/bin/warehouse-executor" ]
EXPOSE 8080 8000

