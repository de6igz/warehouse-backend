FROM python:3.8

# Установка пакета PostgreSQL клиента
RUN apt-get update && apt-get install -y postgresql-client \
    && rm -rf /var/lib/apt/lists/*

# Копируем скрипт ожидания готовности базы данных в образ
COPY wait-for-db.sh /wait-for-db.sh

RUN set -ex \
    && chmod +x /wait-for-db.sh \
    && pip install psycopg2-binary yandex-pgmigrate==1.0.6

WORKDIR /app

# Устанавливаем скрипт ожидания готовности базы данных как точку входа
ENTRYPOINT ["/wait-for-db.sh"]
