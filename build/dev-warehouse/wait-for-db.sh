#!/bin/bash

# Ожидание доступности базы данных
until PGPASSWORD=warehouse-pg psql -h host.docker.internal -U warehouse-pg -d warehouse-pg -c '\q'; do
  >&2 echo "База данных не доступна, ожидание..."
  sleep 5
done

# База данных доступна, запуск миграций
echo "База данных доступна, запуск миграций..."
pgmigrate -t latest migrate -c "host=host.docker.internal dbname=warehouse-pg user=warehouse-pg password=warehouse-pg"
