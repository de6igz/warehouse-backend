package core

import (
	"common/http"
	"github.com/labstack/echo/v4"
)

type Handler interface {
	// GetMethod получить название метода ручки
	GetMethod() http.Methods
	// GetRoute получить путь ручки
	GetRoute() string
	// Do метод, который вызывается при обращении к ручке
	Do(ctx echo.Context) error
}
