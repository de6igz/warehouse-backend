package utils

const (
	NoProductError     = "ERROR #P0001 No product"
	NoReservationError = "ERROR #P0001 No reservation"
)
