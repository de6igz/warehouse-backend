# Запуск всего процесса
.PHONY: up
up: start_db start_app

# Поднять БД
.PHONY: start_db
start_db:
	@echo "Поднимаем базу данных..."
	docker-compose -f build/dev-warehouse/db/docker-compose.db.yaml up -d

# Запуск приложения с миграциями
.PHONY: start_app
start_app:
	@echo "Запускаем приложение..."
	docker-compose -f build/dev-warehouse/docker-compose.app.yaml up -d

.PHONY: migrate
migrate:
	@echo "Запускаем миграции..."
	docker-compose -f build/dev-warehouse/docker-compose.migrations.yaml up -d

# Остановить приложения и базы данных
.PHONY: stop
stop:
	@echo "Останавливаем приложение и базу данных..."
	docker-compose -f build/dev-warehouse/db/docker-compose.db.yaml down
	docker-compose -f build/dev-warehouse/docker-compose.app.yaml down

# Запустить тесты
.PHONY: run_tests
run_tests:
	go test warehouse-executor/usecases/warehouse_service
	go test warehouse-executor/entrypoints/http/v1/cancel-reserve
	go test warehouse-executor/entrypoints/http/v1/products
	go test warehouse-executor/entrypoints/http/v1/reserve
