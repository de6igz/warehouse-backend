create table if not exists product(
    id serial primary key ,
    name varchar(128) not null,
    size integer not null ,
    unique_code varchar(128) not null unique,
    available_amount integer  CHECK (available_amount >= 0),
    reserved_amount integer CHECK (available_amount >= 0)
);


