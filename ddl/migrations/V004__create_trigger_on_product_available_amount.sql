CREATE OR REPLACE FUNCTION check_available_amount()
    RETURNS TRIGGER AS $$
BEGIN
    IF NEW.available_amount < 0 THEN
        RAISE EXCEPTION 'No product';
END IF;

RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER before_update_available_amount
    BEFORE UPDATE OF available_amount ON product
    FOR EACH ROW
    EXECUTE FUNCTION check_available_amount();