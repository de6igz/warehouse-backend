create table if not exists warehouse(
    id serial primary key ,
    name varchar(128) not null ,
    availability bool default true
);