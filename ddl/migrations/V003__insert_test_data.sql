INSERT INTO warehouse (name, availability) VALUES
                                               ('Warehouse A', true),
                                               ('Warehouse B', true),
                                               ('Warehouse C', false),
                                               ('Warehouse D', true);

INSERT INTO product (name, size, unique_code, available_amount, reserved_amount) VALUES
                                                                                     ('Product 1', 10, 'code1', 20, 5),
                                                                                     ('Product 2', 15, 'code2', 30, 10),
                                                                                     ('Product 3', 8, 'code3', 15, 3),
                                                                                     ('Product 4', 20, 'code4', 1, 0);

