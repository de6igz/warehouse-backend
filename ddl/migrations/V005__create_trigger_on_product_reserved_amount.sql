CREATE OR REPLACE FUNCTION check_reserved_amount()
    RETURNS TRIGGER AS $$
BEGIN
    IF NEW.reserved_amount < 0 THEN
        RAISE EXCEPTION 'No reservation';
END IF;

RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER before_update_reserved_amount
    BEFORE UPDATE OF reserved_amount ON product
    FOR EACH ROW
    EXECUTE FUNCTION check_reserved_amount();