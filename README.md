# warehouse-backend






## Поднять в Docker 
```bash
make up
```
```make up``` - Поднимает БД в Docker. Затем запускает миграции и API <br>
```make start_db``` - Поднимает БД в Docker <br>
```make start_app``` - Запускает миграции и API <br>
```make stop``` - Останавливает все контейнеры <br>
```make migrate``` - Запускает миграции


## Поднять локально:
#### 1) Поднять базу данных
```bash 
make start_db
``` 
#### 2) Запустить миграции
```bash 
make migrate
```
#### 3)  Выставить  env для указания пути конфига
```
CONFIG_PATH=/path/to/config/config.yaml
```



#### 4) Запустить main.go в executor
## Запустить тесты
Чтобы запустить тесты для сервиса warehouse-executor, нужно установить все зависимости из <b>warehouse-executor/go.mod</b><br> Далее запустить команду:
```bash
make run_tests
```

