package cancel_reserve

import (
	"common/core"
	httpUtils "common/http"
	"common/utils"
	"encoding/json"
	"github.com/labstack/echo/v4"
	"net/http"
	"warehouse-executor/usecases/warehouse_service"
)

const (
	Route  = "/v1/cancel-reserve"
	Method = httpUtils.PostMethod
)

//go:generate go run github.com/vektra/mockery/v2@v2.42.1 --name=providerCancelReserve
type providerCancelReserve interface {
	GetWarehouseServiceFactory() warehouse_service.WarehouseServiceFactory
}

type cancelReserve struct {
	method    httpUtils.Methods
	route     string
	providers providerCancelReserve
}

func NewCancelReserve(
	method httpUtils.Methods,
	route string,
	providers providerCancelReserve,
) core.Handler {
	return &cancelReserve{
		method:    method,
		route:     route,
		providers: providers,
	}
}

func (c *cancelReserve) GetMethod() httpUtils.Methods {
	return c.method
}

func (c *cancelReserve) GetRoute() string {
	return c.route
}

// Do метод, который вызывается при обращении к ручке
// @Summary     Отменить резерв товаров на складе
// @Produce      json
// @Param requestBody body DtoIn true "Тело запроса"
// @Success      200
// @Router       /v1/cancel-reserve [post]
func (c *cancelReserve) Do(ctx echo.Context) error {
	body := ctx.Request().Body
	var data DtoIn
	err := json.NewDecoder(body).Decode(&data)
	if err != nil {
		return httpUtils.ReturnInternalError(ctx, err, "")
	}
	warehouseService := c.providers.GetWarehouseServiceFactory().GetService()
	errorProduct, err := warehouseService.CancelProductsReservation(ctx.Request().Context(), warehouse_service.CancelReserveDto{ProductCodes: data.ProductCodes})

	if err != nil {
		if err.Error() == utils.NoReservationError {
			return httpUtils.ReturnBadRequestError(ctx, err, "Товар "+errorProduct+" не зарезервирован")
		}
		return httpUtils.ReturnInternalError(ctx, err, "")
	}

	return ctx.NoContent(http.StatusOK)
}
