package cancel_reserve_test

import (
	"bytes"
	httpUtils "common/http"
	"context"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"testing"
	cancelReserve "warehouse-executor/entrypoints/http/v1/cancel-reserve"
	mockProviders "warehouse-executor/providers/mocks"
	usecase "warehouse-executor/usecases/warehouse_service"
	mockUsecase "warehouse-executor/usecases/warehouse_service/mocks"
)

type testingObject struct {
	*testing.T
}

func TestDo_SuccessfulCancelReserve(t *testing.T) {
	// Создание мока сервиса склада
	testObject := &testingObject{T: t}
	mockWarehouseService := mockUsecase.NewWarehouseInventoryService(testObject)
	mockWarehouseService.On("CancelProductsReservation", context.Background(), usecase.CancelReserveDto{
		ProductCodes: []string{"code1", "code2", "code3"},
	}).Return(
		"", nil)

	mockPr := mockProviders.NewExecutorProviders(testObject)
	mockFactory := mockUsecase.NewWarehouseServiceFactory(testObject)
	mockPr.On("GetWarehouseServiceFactory").Return(mockFactory)
	mockFactory.On("GetService").Return(mockWarehouseService)

	handler := cancelReserve.NewCancelReserve(httpUtils.PostMethod, "/v1/cancel-reserve", mockPr)

	// Создание тестового контекста Echo
	e := echo.New()
	req := httptest.NewRequest(http.MethodPost, "/v1/cancel-reserve", bytes.NewBufferString(`{"product_codes": ["code1", "code2","code3"]}`))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	// Выполнение запроса
	err := handler.Do(c)

	// Проверка, что запрос завершился успешно
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, rec.Code)
}
