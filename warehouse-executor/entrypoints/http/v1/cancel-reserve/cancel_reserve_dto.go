package cancel_reserve

// DtoIn Входные данные для ручкуи /cancel-reserve
type DtoIn struct {
	ProductCodes []string `json:"product_codes"`
}
