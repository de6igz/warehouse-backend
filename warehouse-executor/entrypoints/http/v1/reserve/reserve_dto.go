package reserve

// DtoIn Входные данные для ручкуи /reserve
type DtoIn struct {
	ProductCodes []string `json:"product_codes"`
}
