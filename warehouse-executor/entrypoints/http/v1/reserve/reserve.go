package reserve

import (
	"common/core"
	httpUtils "common/http"
	"common/utils"
	"encoding/json"
	"github.com/labstack/echo/v4"
	"net/http"
	"warehouse-executor/usecases/warehouse_service"
)

const (
	Route  = "/v1/reserve"
	Method = httpUtils.PostMethod
)

type providerReserve interface {
	GetWarehouseServiceFactory() warehouse_service.WarehouseServiceFactory
}

type reserve struct {
	method    httpUtils.Methods
	route     string
	providers providerReserve
}

func NewReserve(
	method httpUtils.Methods,
	route string,
	providers providerReserve,
) core.Handler {
	return &reserve{
		method:    method,
		route:     route,
		providers: providers,
	}
}

func (r *reserve) GetMethod() httpUtils.Methods {
	return r.method
}

func (r *reserve) GetRoute() string {
	return r.route
}

// Do метод, который вызывается при обращении к ручке
// @Summary     Зарезервировать товары на складе
// @Produce      json
// @Param requestBody body DtoIn true "Тело запроса"
// @Success      200
// @Router       /v1/reserve [post]
func (r *reserve) Do(ctx echo.Context) error {
	body := ctx.Request().Body
	var data DtoIn
	err := json.NewDecoder(body).Decode(&data)
	if err != nil {
		return httpUtils.ReturnInternalError(ctx, err, "")
	}
	warehouseService := r.providers.GetWarehouseServiceFactory().GetService()
	errorProduct, err := warehouseService.ReserveProducts(ctx.Request().Context(), warehouse_service.ReserveDto{ProductCodes: data.ProductCodes})

	if err != nil {
		if err.Error() == utils.NoProductError {
			return httpUtils.ReturnBadRequestError(ctx, err, "Товар "+errorProduct+" закончился на складе")
		}
		return httpUtils.ReturnInternalError(ctx, err, "")
	}

	return ctx.NoContent(http.StatusOK)
}
