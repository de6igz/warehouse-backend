package products

import (
	"common/core"
	httpUtils "common/http"
	"github.com/labstack/echo/v4"
	"net/http"
	"warehouse-executor/usecases/warehouse_service"
)

const (
	Route  = "/v1/products"
	Method = httpUtils.GetMethod
)

type providerProducts interface {
	GetWarehouseServiceFactory() warehouse_service.WarehouseServiceFactory
}

type products struct {
	method    httpUtils.Methods
	route     string
	providers providerProducts
}

func NewProducts(
	method httpUtils.Methods,
	route string,
	providers providerProducts,
) core.Handler {
	return &products{
		method:    method,
		route:     route,
		providers: providers,
	}
}

func (p *products) GetMethod() httpUtils.Methods {
	return p.method
}

func (p *products) GetRoute() string {
	return p.route
}

// Do метод, который вызывается при обращении к ручке
// @Summary     Получить товары оставшиеся на складе
// @Produce      json
// @Param        limit    query     string  false  "limit"
// @Param        offset    query     string  false  "limit"
// @Success      200 {object} DtoOut
// @Router       /v1/products [get]
func (p *products) Do(ctx echo.Context) error {

	limit := ctx.FormValue("limit")
	offset := ctx.FormValue("offset")

	warehouseService := p.providers.GetWarehouseServiceFactory().GetService()
	data, err := warehouseService.GetProducts(ctx.Request().Context(), limit, offset)

	if err != nil {
		return httpUtils.ReturnInternalError(ctx, err, "")
	}

	return ctx.JSON(http.StatusOK, prepareData(data))
}

func prepareData(data warehouse_service.ProductsDtoOut) DtoOut {
	res := make([]Product, 0, len(data.Items))

	for _, item := range data.Items {
		res = append(res, Product{
			Name:   item.Name,
			Size:   item.Size,
			Code:   item.Code,
			Amount: item.Amount,
		})
	}

	return DtoOut{Items: res,
		Pagination: Pagination{
			Limit:  data.Pagination.Limit,
			Offset: data.Pagination.Offset,
			Total:  data.Pagination.Total,
		},
	}
}
