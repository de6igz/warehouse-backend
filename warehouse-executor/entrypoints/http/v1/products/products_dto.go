package products

// DtoOut Выходные данные
type DtoOut struct {
	Items      []Product  `json:"items"`
	Pagination Pagination `json:"pagination"`
}

// Product Экземпляр одного продукта
type Product struct {
	Name   string `json:"name"`
	Size   int    `json:"size"`
	Code   string `json:"product_code"`
	Amount int    `json:"amount"`
}

// Pagination Пагинация
type Pagination struct {
	Limit  int `json:"limit"`
	Offset int `json:"offset"`
	Total  int `json:"total"`
}
