package products

import (
	"bytes"
	httpUtils "common/http"
	"context"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"testing"
	mockProviders "warehouse-executor/providers/mocks"
	"warehouse-executor/usecases/warehouse_service"
	mockUsecase "warehouse-executor/usecases/warehouse_service/mocks"
)

type testingObject struct {
	*testing.T
}

func TestDo_SuccessfulProducts(t *testing.T) {
	// Создание мока сервиса склада
	testObject := &testingObject{T: t}
	mockWarehouseService := mockUsecase.NewWarehouseInventoryService(testObject)
	mockWarehouseService.On("GetProducts", context.Background(), "", "").Return(
		warehouse_service.ProductsDtoOut{
			Items: []warehouse_service.Product{{
				Name:   "Product1",
				Size:   0,
				Code:   "code1",
				Amount: 0,
			}},
			Pagination: warehouse_service.Pagination{
				Limit:  20,
				Offset: 0,
				Total:  25,
			},
		}, nil)

	mockPr := mockProviders.NewExecutorProviders(testObject)
	mockFactory := mockUsecase.NewWarehouseServiceFactory(testObject)
	mockPr.On("GetWarehouseServiceFactory").Return(mockFactory)
	mockFactory.On("GetService").Return(mockWarehouseService)

	handler := NewProducts(httpUtils.GetMethod, "/v1/products", mockPr)

	// Создание тестового контекста Echo
	e := echo.New()
	req := httptest.NewRequest(http.MethodGet, "/v1/products", bytes.NewBufferString(``))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	// Выполнение запроса
	err := handler.Do(c)

	// Проверка, что запрос завершился успешно
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, rec.Code)
}
