package main

import (
	"common/core"
	"common/http/middleware"
	logger "common/log"
	"github.com/labstack/echo/v4"
	cancel_reserve "warehouse-executor/entrypoints/http/v1/cancel-reserve"
	"warehouse-executor/entrypoints/http/v1/products"
	"warehouse-executor/entrypoints/http/v1/reserve"
	dataproviders "warehouse-executor/providers"
)

func main() {
	config := core.NewConfig("test")
	err := config.LoadConfig()
	if err != nil {
		logger.Fatalf("Error occurred while loading config: %+v, config: %+v", err, config)

	}
	providers, err := dataproviders.NewProviders(config)
	if err != nil {
		logger.Fatalf("%s", err)
	}

	mw := middleware.NewBaseMiddleware()
	service := echo.New()

	microservice := core.NewMicroservice(config, service, mw.GetGlobalMiddlewares())
	logger.InitLog(config.GetLogConfig())

	addRoutes(config, providers)

	microservice.Run()
}

func addRoutes(
	config core.Config,
	providers dataproviders.ExecutorProviders) {

	config.AddHandler(reserve.NewReserve(reserve.Method, reserve.Route, providers)).
		AddHandler(cancel_reserve.NewCancelReserve(cancel_reserve.Method, cancel_reserve.Route, providers)).
		AddHandler(products.NewProducts(products.Method, products.Route, providers))

}
