package pg_provider

import (
	"common/core"
	"context"
	"fmt"
	"github.com/go-pg/pg/v10"
)

//go:generate go run github.com/vektra/mockery/v2@v2.42.1 --name=Postgres
type Postgres interface {
	InitPg() error
	// UpdateReserve зарезервировать товар
	UpdateReserve(ctx context.Context, product Product) error
	// UpdateCancelReserve Отменить резервацию продукта
	UpdateCancelReserve(ctx context.Context, product Product) error
	// GetWarehouseAvailability Получить доступность склада
	GetWarehouseAvailability(ctx context.Context) (bool, error)
	// GetProducts Получить товары оставшиеся на складе
	GetProducts(ctx context.Context) ([]Product, error)
}
type postgres struct {
	cfg core.PostgresConfig
	db  *pg.DB
}

// NewPostgres получить новый интерфейс для работы с БД
func NewPostgres(cfg core.PostgresConfig) Postgres {
	return &postgres{cfg: cfg}
}

// InitPg инициализация соединения
func (p *postgres) InitPg() error {
	p.db = pg.Connect(&pg.Options{
		Addr:            p.cfg.Host + ":" + p.cfg.Port,
		User:            p.cfg.UserName,
		Password:        p.cfg.Password,
		Database:        p.cfg.DBName,
		MaxRetries:      3,
		MaxRetryBackoff: 3,
	})

	ctx := context.Background()

	if err := p.db.Ping(ctx); err != nil {
		p.logConnection(err)

		return err
	}

	p.logConnection(nil)

	return nil
}

func (p *postgres) logConnection(err error) {
	s := "no error"
	if err != nil {
		s = fmt.Sprintf("%s", err)
	}

	fmt.Printf("%s@%s:%s:%s/%s connection error:%s", p.cfg.UserName, p.cfg.Password,
		p.cfg.Host, p.cfg.Port, p.cfg.DBName, s)
}

// UpdateReserve зарезервировать товар
func (p *postgres) UpdateReserve(ctx context.Context, product Product) error {
	tx, err := p.db.Begin()
	if err != nil {
		return err
	}
	defer tx.Rollback()

	_, err = tx.ExecContext(ctx, updateReserve, product.Code)
	if err != nil {
		return err
	}

	return tx.Commit()
}

// GetWarehouseAvailability Получить доступность склада
func (p *postgres) GetWarehouseAvailability(ctx context.Context) (bool, error) {
	res := WarehouseAvailability{}

	_, err := p.db.QueryContext(ctx, &res, selectAvailability)
	if err != nil {
		return false, err
	}

	if err != nil {
		return false, err
	}

	return res.IsAvailability, nil
}

// UpdateCancelReserve Отменить резервацию продукта
func (p *postgres) UpdateCancelReserve(ctx context.Context, product Product) error {
	tx, err := p.db.Begin()
	if err != nil {
		return err
	}
	defer tx.Rollback()

	_, err = tx.ExecContext(ctx, updateCancelReserve, product.Code)
	if err != nil {
		return err
	}

	return tx.Commit()
}

// GetProducts Получить товары оставшиеся на складе
func (p *postgres) GetProducts(ctx context.Context) ([]Product, error) {
	res := make([]Product, 0, 32)

	_, err := p.db.QueryContext(ctx, &res, selectAvailableProducts)

	if err != nil {
		return nil, err
	}

	return res, nil
}
