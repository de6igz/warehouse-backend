package pg_provider

const (
	selectAvailability      = `select availability from warehouse where id = 1;`
	selectAvailableProducts = `select name,size,unique_code,available_amount from product where available_amount >0`
)

const (
	updateReserve = `UPDATE product
					 SET reserved_amount = reserved_amount + 1, available_amount = available_amount - 1
					 WHERE unique_code = ? ;`

	updateCancelReserve = `UPDATE product
					 SET reserved_amount = reserved_amount - 1, available_amount = available_amount + 1
					 WHERE unique_code = ? ;`
)

type WarehouseAvailability struct {
	IsAvailability bool `pg:"availability"`
}

// Product Экземпляр одного продукта
type Product struct {
	Name   string `pg:"name"`
	Size   int    `pg:"size"`
	Code   string `pg:"unique_code"`
	Amount int    `pg:"available_amount"`
}
