package providers

import (
	"common/core"
	"warehouse-executor/providers/pg_provider"
	"warehouse-executor/usecases/warehouse_service"
)

//go:generate go run github.com/vektra/mockery/v2@v2.42.1 --name=ExecutorProviders
type ExecutorProviders interface {
	// GetWarehouseServiceFactory получить фабрику для работы с логикой пользователя
	GetWarehouseServiceFactory() warehouse_service.WarehouseServiceFactory
}
type executorProviders struct {
	warehouseFactory warehouse_service.WarehouseServiceFactory
	pg               pg_provider.Postgres
}

func (p *executorProviders) GetWarehouseServiceFactory() warehouse_service.WarehouseServiceFactory {
	return p.warehouseFactory
}

// NewProviders инициализация провайдеров
func NewProviders(config core.Config) (ExecutorProviders, error) {
	pg := pg_provider.NewPostgres(config.GetPostgresConfig())
	if err := pg.InitPg(); err != nil {
		return nil, err
	}

	warehouseFactory := warehouse_service.NewWarehouseFactory(pg)

	return &executorProviders{

		warehouseFactory: warehouseFactory,
		pg:               pg,
	}, nil
}
