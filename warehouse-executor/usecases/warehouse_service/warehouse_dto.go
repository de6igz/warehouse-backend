package warehouse_service

import (
	"context"
	"warehouse-executor/providers/pg_provider"
)

type warehouseServicePostgres interface {
	// UpdateReserve зарезервировать товар
	UpdateReserve(ctx context.Context, product pg_provider.Product) error
	// UpdateCancelReserve Отменить резервацию продукта
	UpdateCancelReserve(ctx context.Context, product pg_provider.Product) error
	// GetWarehouseAvailability Получить доступность склада
	GetWarehouseAvailability(ctx context.Context) (bool, error)
	// GetProducts Получить товары оставшиеся на складе
	GetProducts(ctx context.Context) ([]pg_provider.Product, error)
}

type warehouseService struct {
	//Провайдеры
	pg warehouseServicePostgres
}

// ReserveDto коды продуктов для резервирования
type ReserveDto struct {
	ProductCodes []string
}

// CancelReserveDto  коды продуктов для отмены резервирования
type CancelReserveDto struct {
	ProductCodes []string
}

// ProductsDtoOut  Выходные данные
type ProductsDtoOut struct {
	Items      []Product
	Pagination Pagination
}

// Product Экземпляр одного продукта
type Product struct {
	Name   string
	Size   int
	Code   string
	Amount int
}

// Pagination Пагинация
type Pagination struct {
	Limit  int
	Offset int
	Total  int
}
