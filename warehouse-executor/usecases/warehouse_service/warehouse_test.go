package warehouse_service

import (
	"context"
	"fmt"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"testing"
	"warehouse-executor/providers/pg_provider"
	mockPg "warehouse-executor/providers/pg_provider/mocks"
)

type testingObject struct {
	*testing.T
}

func TestWarehouseInventoryService(t *testing.T) {
	t.Run("TestReserveProducts", TestReserveProducts)
	t.Run("TestCancelProductsReservation", TestCancelProductsReservation)
	t.Run("TestGetProducts", TestGetProducts)
}

type testCase struct {
	name string
	test func(t *testing.T)
}

func TestReserveProducts(t *testing.T) {
	testCases := make([]testCase, 0, 3)

	testCases = []testCase{
		{
			name: "Успех",
			test: func(t *testing.T) {
				testObject := &testingObject{T: t}

				postgresMock := mockPg.NewPostgres(testObject)

				service := warehouseService{
					pg: postgresMock,
				}

				postgresMock.On("GetWarehouseAvailability", context.Background()).Return(true, nil)
				postgresMock.On("UpdateReserve", context.Background(), mock.Anything).Return(nil)

				args := []string{"code1", "code2", "code3"}
				_, err := service.ReserveProducts(context.Background(), ReserveDto{ProductCodes: args})
				assert.NoError(t, err)
			},
		},
		{
			name: "Склад недоступен",
			test: func(t *testing.T) {
				testObject := &testingObject{T: t}

				postgresMock := mockPg.NewPostgres(testObject)

				service := warehouseService{
					pg: postgresMock,
				}

				postgresMock.On("GetWarehouseAvailability", context.Background()).Return(false, nil)

				args := []string{"code1", "code2", "code3"}
				_, err := service.ReserveProducts(context.Background(), ReserveDto{ProductCodes: args})
				assert.EqualError(t, err, "склад не доступен в настоящее время")
			},
		},
		{
			name: "Ошибка резервирования товара",
			test: func(t *testing.T) {
				testObject := &testingObject{T: t}

				postgresMock := mockPg.NewPostgres(testObject)

				service := warehouseService{
					pg: postgresMock,
				}

				postgresMock.On("GetWarehouseAvailability", context.Background()).Return(true, nil)
				postgresMock.On("UpdateReserve", context.Background(), pg_provider.Product{Code: "code1"}).Return(nil)
				postgresMock.On("UpdateReserve", context.Background(), pg_provider.Product{Code: "code2"}).Return(nil)
				postgresMock.On("UpdateReserve", context.Background(), pg_provider.Product{Code: "code3"}).Return(fmt.Errorf("ERROR #P0001 No product"))

				args := []string{"code1", "code2", "code3", "code4"}
				_, err := service.ReserveProducts(context.Background(), ReserveDto{ProductCodes: args})
				assert.EqualError(t, err, "ERROR #P0001 No product")
			},
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, tc.test)
	}

}

func TestCancelProductsReservation(t *testing.T) {
	testCases := make([]testCase, 0, 3)

	testCases = []testCase{
		{
			name: "Успех",
			test: func(t *testing.T) {
				testObject := &testingObject{T: t}

				postgresMock := mockPg.NewPostgres(testObject)

				service := warehouseService{
					pg: postgresMock,
				}

				postgresMock.On("GetWarehouseAvailability", context.Background()).Return(true, nil)
				postgresMock.On("UpdateCancelReserve", context.Background(), mock.Anything).Return(nil)

				args := []string{"code1", "code2", "code3"}
				_, err := service.CancelProductsReservation(context.Background(), CancelReserveDto{ProductCodes: args})
				assert.NoError(t, err)
			},
		},
		{
			name: "Склад недоступен",
			test: func(t *testing.T) {
				testObject := &testingObject{T: t}

				postgresMock := mockPg.NewPostgres(testObject)

				service := warehouseService{
					pg: postgresMock,
				}

				postgresMock.On("GetWarehouseAvailability", context.Background()).Return(false, nil)

				args := []string{"code1", "code2", "code3"}
				_, err := service.CancelProductsReservation(context.Background(), CancelReserveDto{ProductCodes: args})
				assert.EqualError(t, err, "склад не доступен в настоящее время")
			},
		},
		{
			name: "Ошибка отмены резервации товара",
			test: func(t *testing.T) {
				testObject := &testingObject{T: t}

				postgresMock := mockPg.NewPostgres(testObject)

				service := warehouseService{
					pg: postgresMock,
				}

				postgresMock.On("GetWarehouseAvailability", context.Background()).Return(true, nil)
				postgresMock.On("UpdateCancelReserve", context.Background(), pg_provider.Product{Code: "code1"}).Return(nil)
				postgresMock.On("UpdateCancelReserve", context.Background(), pg_provider.Product{Code: "code2"}).Return(nil)
				postgresMock.On("UpdateCancelReserve", context.Background(), pg_provider.Product{Code: "code3"}).Return(fmt.Errorf("ERROR #P0001 No reservation"))

				args := []string{"code1", "code2", "code3", "code4"}
				_, err := service.CancelProductsReservation(context.Background(), CancelReserveDto{ProductCodes: args})
				assert.EqualError(t, err, "ERROR #P0001 No reservation")
			},
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, tc.test)
	}
}

func TestGetProducts(t *testing.T) {
	testCases := make([]testCase, 0, 6)

	testCases = []testCase{
		{
			name: "Успех",
			test: func(t *testing.T) {
				testObject := &testingObject{T: t}

				postgresMock := mockPg.NewPostgres(testObject)

				service := warehouseService{
					pg: postgresMock,
				}

				postgresMock.On("GetProducts", context.Background()).Return([]pg_provider.Product{
					{mock.Anything, 0, mock.Anything, 1},
					{mock.Anything, 0, mock.Anything, 1},
					{mock.Anything, 0, mock.Anything, 1},
					{mock.Anything, 0, mock.Anything, 1}}, nil)

				data, err := service.GetProducts(context.Background(), "", "2")
				assert.Equal(t, len(data.Items), 2)
				assert.NoError(t, err)
				assert.Equal(t, data.Pagination, Pagination{
					Limit:  20,
					Offset: 2,
					Total:  4,
				})
			},
		},
		{
			name: "Ошибка получения данных из хранилища",
			test: func(t *testing.T) {
				testObject := &testingObject{T: t}

				postgresMock := mockPg.NewPostgres(testObject)

				service := warehouseService{
					pg: postgresMock,
				}

				postgresMock.On("GetProducts", context.Background()).Return(nil, fmt.Errorf("error"))

				_, err := service.GetProducts(context.Background(), "", "2")
				assert.Error(t, err)
			},
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, tc.test)
	}
}
