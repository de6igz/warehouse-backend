package warehouse_service

import (
	"warehouse-executor/providers/pg_provider"
)

// WarehouseServiceFactory интерфейс фабрики
//
//go:generate go run github.com/vektra/mockery/v2@v2.42.1 --name=WarehouseServiceFactory
type WarehouseServiceFactory interface {
	GetService() WarehouseInventoryService
}

type warehouseServiceFactory struct {
	pg pg_provider.Postgres
}

// NewWarehouseFactory  получить новый экземпляр фабрики сервисов
func NewWarehouseFactory(pg pg_provider.Postgres) WarehouseServiceFactory {
	return &warehouseServiceFactory{
		pg: pg,
	}
}

// GetService получить новых экземпляр сервиса
func (c *warehouseServiceFactory) GetService() WarehouseInventoryService {
	return newWarehouseService(c.pg)
}
