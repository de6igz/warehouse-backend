package warehouse_service

import (
	"context"
	"fmt"
	"warehouse-executor/providers/pg_provider"
)

//go:generate go run github.com/vektra/mockery/v2@v2.42.1 --name=WarehouseInventoryService
type WarehouseInventoryService interface {
	// ReserveProducts Зарезервировать продукты
	ReserveProducts(ctx context.Context, data ReserveDto) (string, error)
	// CancelProductsReservation Отменить резервацию продуктов
	CancelProductsReservation(ctx context.Context, data CancelReserveDto) (string, error)
	// GetProducts Получить товары оставшиеся на складе
	GetProducts(ctx context.Context, limit string, offset string) (ProductsDtoOut, error)
}

// ReserveProducts Зарезервировать продукты
func (w *warehouseService) ReserveProducts(ctx context.Context, data ReserveDto) (string, error) {

	isAvailable, err := w.pg.GetWarehouseAvailability(ctx)
	if err != nil {
		return "", err
	}

	if !isAvailable {
		return "", fmt.Errorf("склад не доступен в настоящее время")
	}

	for _, code := range data.ProductCodes {
		err = w.pg.UpdateReserve(ctx, pg_provider.Product{Code: code})
		if err != nil {
			return code, err
		}
	}

	return "", nil

}

// CancelProductsReservation Отменить резервацию продуктов
func (w *warehouseService) CancelProductsReservation(ctx context.Context, data CancelReserveDto) (string, error) {
	isAvailable, err := w.pg.GetWarehouseAvailability(ctx)
	if err != nil {
		return "", err
	}

	if !isAvailable {
		return "", fmt.Errorf("склад не доступен в настоящее время")
	}

	for _, code := range data.ProductCodes {
		err = w.pg.UpdateCancelReserve(ctx, pg_provider.Product{Code: code})
		if err != nil {
			return code, err
		}
	}

	return "", nil
}

// GetProducts Получить товары оставшиеся на складе
func (w *warehouseService) GetProducts(ctx context.Context, limit string, offset string) (ProductsDtoOut, error) {
	data, err := w.pg.GetProducts(ctx)

	if err != nil {
		return ProductsDtoOut{}, err
	}

	if limit == "" {
		limit = "20"
	}
	if offset == "" {
		offset = "0"
	}

	transformedData := make([]Product, 0, len(data))

	for _, product := range data {
		transformedData = append(transformedData, Product{
			Name:   product.Name,
			Size:   product.Size,
			Code:   product.Code,
			Amount: product.Amount,
		})
	}

	res, err := paginateProducts(transformedData, offset, limit)

	return ProductsDtoOut{
		Items:      res.Items,
		Pagination: res.Pagination,
	}, nil

}
