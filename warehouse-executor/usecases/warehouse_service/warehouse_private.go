package warehouse_service

import "strconv"

func newWarehouseService(pg warehouseServicePostgres) WarehouseInventoryService {
	return &warehouseService{
		pg: pg,
	}
}

func paginateProducts(data []Product, off string, lim string) (ProductsDtoOut, error) {
	totalCount := len(data)
	offset, err := strconv.Atoi(off)
	if err != nil {
		return ProductsDtoOut{}, err
	}

	limit, err := strconv.Atoi(lim)
	if err != nil {
		return ProductsDtoOut{}, err
	}

	start := offset
	end := offset + limit

	// Убедимся, что начальный индекс не превышает длину среза
	if start > len(data) {
		start = len(data)
	}

	// Убедимся, что конечный индекс не превышает длину среза
	if end > len(data) {
		end = len(data)
	}

	return ProductsDtoOut{
		Items: data[start:end],
		Pagination: Pagination{
			Limit:  limit,
			Offset: offset,
			Total:  totalCount,
		},
	}, nil
}
