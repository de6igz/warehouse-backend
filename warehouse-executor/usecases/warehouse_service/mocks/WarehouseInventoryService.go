// Code generated by mockery v2.42.1. DO NOT EDIT.

package mocks

import (
	context "context"
	warehouse_service "warehouse-executor/usecases/warehouse_service"

	mock "github.com/stretchr/testify/mock"
)

// WarehouseInventoryService is an autogenerated mock type for the WarehouseInventoryService type
type WarehouseInventoryService struct {
	mock.Mock
}

// CancelProductsReservation provides a mock function with given fields: ctx, data
func (_m *WarehouseInventoryService) CancelProductsReservation(ctx context.Context, data warehouse_service.CancelReserveDto) (string, error) {
	ret := _m.Called(ctx, data)

	if len(ret) == 0 {
		panic("no return value specified for CancelProductsReservation")
	}

	var r0 string
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, warehouse_service.CancelReserveDto) (string, error)); ok {
		return rf(ctx, data)
	}
	if rf, ok := ret.Get(0).(func(context.Context, warehouse_service.CancelReserveDto) string); ok {
		r0 = rf(ctx, data)
	} else {
		r0 = ret.Get(0).(string)
	}

	if rf, ok := ret.Get(1).(func(context.Context, warehouse_service.CancelReserveDto) error); ok {
		r1 = rf(ctx, data)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetProducts provides a mock function with given fields: ctx, limit, offset
func (_m *WarehouseInventoryService) GetProducts(ctx context.Context, limit string, offset string) (warehouse_service.ProductsDtoOut, error) {
	ret := _m.Called(ctx, limit, offset)

	if len(ret) == 0 {
		panic("no return value specified for GetProducts")
	}

	var r0 warehouse_service.ProductsDtoOut
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, string, string) (warehouse_service.ProductsDtoOut, error)); ok {
		return rf(ctx, limit, offset)
	}
	if rf, ok := ret.Get(0).(func(context.Context, string, string) warehouse_service.ProductsDtoOut); ok {
		r0 = rf(ctx, limit, offset)
	} else {
		r0 = ret.Get(0).(warehouse_service.ProductsDtoOut)
	}

	if rf, ok := ret.Get(1).(func(context.Context, string, string) error); ok {
		r1 = rf(ctx, limit, offset)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// ReserveProducts provides a mock function with given fields: ctx, data
func (_m *WarehouseInventoryService) ReserveProducts(ctx context.Context, data warehouse_service.ReserveDto) (string, error) {
	ret := _m.Called(ctx, data)

	if len(ret) == 0 {
		panic("no return value specified for ReserveProducts")
	}

	var r0 string
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, warehouse_service.ReserveDto) (string, error)); ok {
		return rf(ctx, data)
	}
	if rf, ok := ret.Get(0).(func(context.Context, warehouse_service.ReserveDto) string); ok {
		r0 = rf(ctx, data)
	} else {
		r0 = ret.Get(0).(string)
	}

	if rf, ok := ret.Get(1).(func(context.Context, warehouse_service.ReserveDto) error); ok {
		r1 = rf(ctx, data)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// NewWarehouseInventoryService creates a new instance of WarehouseInventoryService. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
// The first argument is typically a *testing.T value.
func NewWarehouseInventoryService(t interface {
	mock.TestingT
	Cleanup(func())
}) *WarehouseInventoryService {
	mock := &WarehouseInventoryService{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
